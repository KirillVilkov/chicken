//
//  Application.hpp
//  Chicken
//
//  Created by Кирилл on 22.03.15.
//
//

#ifndef Chicken_Application_hpp
#define Chicken_Application_hpp


namespace civ2 {
    
    class Window;
    
    class Application {
    private:
        bool running;
        Window *_window;
    public:
        Application();
        void run();
        void quit();
        
        inline Window *window() { return _window; }
    };
    
}

#endif
