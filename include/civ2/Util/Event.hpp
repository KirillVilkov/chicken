//
//  Event.h
//  Chicken
//
//  Created by Кирилл on 22.03.15.
//
//

#ifndef Chicken_Event_hpp
#define Chicken_Event_hpp

#include <functional>
#include <list>

namespace civ2 {
    
    template<typename R, typename ... Args>
    class Event;
    
    template<typename R, typename ... Args>
    class Event< R (Args...) > {
    private:
        struct Subscription {
            void *owner;
            std::function<R(Args...)> callback;
        };
        std::list< Subscription > subscriptions;
    public:
        void subscribe(void *owner, std::function<R(Args...)> callback) {
            Subscription subscription = { owner, callback };
            subscriptions.push_back(subscription);
        }
        
        void unsubscribe(void *owner) {
            for (typename std::list<Subscription>::iterator it = subscriptions.begin(); it != subscriptions.end();) {
                if (it->owner == owner) {
                    it = subscriptions.erase(it);
                } else {
                    ++it;
                }
            }
        }
        
        R fire(Args... args) {
            for (Subscription& subscription : subscriptions) {
                subscription.callback(args...);
            }
        }
    };
    
}

#endif
