//
//  Window.hpp
//  ChickenGfx
//
//  Created by Кирилл on 20.03.15.
//
//

#ifndef ChickenGfx_Window_hpp
#define ChickenGfx_Window_hpp

#include <stdint.h>
#include <civ2/Util/Event.hpp>

struct GLFWwindow;

namespace civ2 {
    
    class Window {
    private:
        GLFWwindow * window;
    public:
        void open(uint32_t width, uint32_t height);
        void close();
        void update();
        Event<void()> onCloseRequested;
    };
    
}

#endif
