//
//  Window.cpp
//  ChickenGfx
//
//  Created by Кирилл on 20.03.15.
//
//

#include <civ2/Util/Window.hpp>
#include <GLFW/glfw3.h>

namespace civ2 {

    void Window::open(uint32_t width, uint32_t height) {
        glfwInit();
        window = glfwCreateWindow(width, height, "Civ 2", NULL, NULL);
    }
    
    void Window::close() {
        glfwTerminate();
    }
    
    void Window::update() {
        glfwSwapBuffers(window);
        glfwPollEvents();
        if (glfwWindowShouldClose(window)) {
            onCloseRequested.fire();
        }
    }

}