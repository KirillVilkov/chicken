#include <civ2/App/Application.hpp>
#include <civ2/Util/Window.hpp>

using namespace civ2;

int main() {
    
    Application *app = new Application;
    
    app->window()->onCloseRequested.subscribe(app, [app]() {
        app->quit();
    });
    
    app->run();
    
    return 0;
}