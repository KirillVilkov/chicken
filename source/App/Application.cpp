//
//  Application.cpp
//  Chicken
//
//  Created by Кирилл on 22.03.15.
//
//

#include <civ2/App/Application.hpp>
#include <civ2/Util/Window.hpp>

namespace civ2 {

    Application::Application() {
        running = false;
        _window = new Window();
        _window->open(800, 600);
    }
    
    void Application::run() {
        running = true;
        while (running) {
            _window->update();
        }
        _window->close();
    }
    
    void Application::quit() {
        running = false;
    }

}