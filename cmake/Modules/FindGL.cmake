# - Try to find OpenGL
# Once done this will define:
#  GL_FOUND        - system has OpenGL and glut
#  GL_INCLUDE_DIR  - incude paths to use OpenGL and glut
#  GL_LIBRARIES    - Link these to use OpenGL and glut

SET(GL_FOUND 0)


IF (APPLE)
	SET (GL_FOUND "YES")
	SET (GL_LIBRARIES "-framework AGL -framework OpenGL")
ENDIF()

IF (WIN32)
	SET (GL_FOUND "YES")
	SET (GL_LIBRARIES "OpenGL32")
ENDIF()